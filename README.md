# Docker Speed Comparison: Alpine vs Debian

## What's the point?
This repository provides a speed comparison for the task of installing the Rails gem in both Alpine Linux and Debian Linux.

## How can I run this on my local machine?
* If you haven't already done so, install Docker on your local machine.
* Then enter the following commands:
```
git clone https://gitlab.com/rubyonracetracks/docker-speed-compare.git
cd docker-speed-compare
```
* To run the Alpine test, enter "bash alpine.sh".
* To run the Debian test, enter "bash debian.sh"

## Results
* The "gem install rails" command took about 38 seconds in Debian Linux but 1 minute and 1 second in Alpine Linux.  Thus, Debian was actually significantly faster.
* On the other hand, provisioning the Docker container was faster in Alpine Linux.  It took 2 minutes and 42 seconds to provision the Docker container AND install Rails in Debian.  It took just 2 minutes and 21 seconds to provision the Docker container AND install Rails in Alpine.

## Conclusions
* If the app you're working on has been Dockerized, it's probably not worth the time and effort to change its Docker setup to a different Linux distro.
* If the app you're working on has not been Dockerized, the Linux distro to use is largely a matter of personal taste.
